package com.hellospringmvc.controller;

import com.hellospringmvc.facade.BookingFacade;
import com.hellospringmvc.model.Event;
import com.hellospringmvc.model.EventImpl;
import com.hellospringmvc.model.Ticket;
import com.hellospringmvc.model.User;
import com.hellospringmvc.model.UserImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

@Controller
@RequestMapping("/")
public class FacadeController {

    private final BookingFacade facade;

    @Autowired
    public FacadeController(BookingFacade facade) {
        this.facade = facade;

        User u1 = new UserImpl();
        u1.setName("User 1");
        u1.setEmail("user_1@email.com");
        u1 = facade.createUser(u1);
        User u2 = new UserImpl();
        u2.setName("User 2");
        u2.setEmail("user_2@email.com");
        u2 = facade.createUser(u2);
        User u3 = new UserImpl();
        u3.setName("User 3");
        u3.setEmail("user_3@email.com");
        u3 = facade.createUser(u3);
        User u4 = new UserImpl();
        u4.setName("User 4");
        u4.setEmail("user_4@email.com");
        u4 = facade.createUser(u4);

        Event e1 = new EventImpl();
        e1.setDate(new Date());
        e1.setTitle("Event 1");
        e1 = facade.createEvent(e1);
        Event e2 = new EventImpl();
        e2.setDate(new Date());
        e2.setTitle("Event 2");
        e2 = facade.createEvent(e2);
        Event e3 = new EventImpl();
        e3.setDate(new Date());
        e3.setTitle("Event 3");
        e3 = facade.createEvent(e3);

        facade.bookTicket(u1.getId(), e1.getId(), 1, Ticket.Category.PREMIUM);
        facade.bookTicket(u2.getId(), e2.getId(), 2, Ticket.Category.STANDARD);
        facade.bookTicket(u3.getId(), e3.getId(), 3, Ticket.Category.BAR);
        facade.bookTicket(u4.getId(), e1.getId(), 123, Ticket.Category.PREMIUM);
    }

    @GetMapping
    public String index(Model model) {
        model.addAttribute("message", "Hello Spring MVC!");
        return "index";
    }

    @GetMapping(value = "/upload-tickets")
    public String uploadTicketsForm() {
        return "upload_tickets";
    }

    @PostMapping(value = "/upload-tickets")
    public ModelAndView uploadFile(@RequestParam("file") MultipartFile xmlFile) throws Exception {
        facade.loadTickets(xmlFile);
        return new ModelAndView("redirect:/tickets?pageSize=100&pageNum=0");
    }

    @GetMapping("/tickets")
    public String getTickets(@RequestParam(name = "pageSize", required = false, defaultValue = "5") Integer pageSize,
                             @RequestParam(name = "pageNum", required = false, defaultValue = "0") Integer pageNum,
                             Model model) {
        model.addAttribute("tickets", facade.getTickets(pageSize, pageNum));
        return "tickets";
    }

    @GetMapping("/tickets/{id}")
    public String getTicketById(@PathVariable Integer id, Model model) {
        buildTicketDetails(id, model);
        return "ticket";
    }

    @GetMapping("/tickets/{id}/pdf")
    public ModelAndView getTicketByIdInPdf(@PathVariable Integer id, Model model) {
        buildTicketDetails(id, model);
        return new ModelAndView("viewPDF", "data", model);
    }

    @GetMapping("/users")
    public String getUsers(@RequestParam(name = "pageSize", required = false, defaultValue = "5") Integer pageSize,
                           @RequestParam(name = "pageNum", required = false, defaultValue = "0") Integer pageNum,
                           Model model) {
        model.addAttribute("users", facade.getUsers(pageSize, pageNum));
        return "users";
    }

    @GetMapping("/events")
    public String getEvents(@RequestParam(name = "pageSize", required = false, defaultValue = "5") Integer pageSize,
                            @RequestParam(name = "pageNum", required = false, defaultValue = "0") Integer pageNum,
                            Model model) {
        model.addAttribute("events", facade.getEvents(pageSize, pageNum));
        return "events";
    }

    @GetMapping("/generate-exception")
    public ModelAndView generateException() {
        int i = 1 / 0;
        return new ModelAndView("redirect:/");
    }


    private void buildTicketDetails(long ticketId, Model model) {
        Ticket ticket = facade.getTicketById(ticketId);
        User user = facade.getUserById(ticket.getUserId());
        Event event = facade.getEventById(ticket.getEventId());
        model.addAttribute("ticket", ticket);
        model.addAttribute("user", user);
        model.addAttribute("event", event);
    }

}

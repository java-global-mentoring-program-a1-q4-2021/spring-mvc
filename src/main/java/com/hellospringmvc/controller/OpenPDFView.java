package com.hellospringmvc.controller;

import com.hellospringmvc.model.Event;
import com.hellospringmvc.model.Ticket;
import com.hellospringmvc.model.User;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Component
public class OpenPDFView extends AbstractPdfView {
    @Override
    protected void buildPdfDocument(Map<String, Object> model,
                                    Document document,
                                    PdfWriter writer,
                                    HttpServletRequest request,
                                    HttpServletResponse response) throws Exception {
        User user = (User) model.get("user");
        Event event = (Event) model.get("event");
        Ticket ticket = (Ticket) model.get("ticket");

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100.0f);
        table.setWidths(new float[]{4.0f, 4.0f});

        table.addCell("Ticket id");
        table.addCell(String.valueOf(ticket.getId()));
        table.addCell("Ticket category");
        table.addCell(ticket.getCategory().toString());
        table.addCell("Ticket place");
        table.addCell(String.valueOf(ticket.getPlace()));

        table.addCell("User id");
        table.addCell(String.valueOf(user.getId()));
        table.addCell("User name");
        table.addCell(user.getName());
        table.addCell("User email");
        table.addCell(user.getEmail());

        table.addCell("Event id");
        table.addCell(String.valueOf(event.getId()));
        table.addCell("Event title");
        table.addCell(event.getTitle());
        table.addCell("Event date");
        table.addCell(event.getDate().toString());

        document.add(table);
    }
}

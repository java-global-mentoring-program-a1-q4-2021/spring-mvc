package com.hellospringmvc.dao;

import com.hellospringmvc.model.EventImpl;
import com.hellospringmvc.model.TicketImpl;
import com.hellospringmvc.model.UserImpl;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Storage {
    private List<UserImpl> users = new ArrayList<>();
    private List<EventImpl> events = new ArrayList<>();
    private List<TicketImpl> tickets = new ArrayList<>();

    public List<UserImpl> getUsers() {
        return users;
    }

    public void setUsers(List<UserImpl> users) {
        this.users = users;
    }

    public List<EventImpl> getEvents() {
        return events;
    }

    public void setEvents(List<EventImpl> events) {
        this.events = events;
    }

    public List<TicketImpl> getTickets() {
        return tickets;
    }

    public void setTickets(List<TicketImpl> tickets) {
        this.tickets = tickets;
    }
}

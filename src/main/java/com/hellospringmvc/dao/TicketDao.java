package com.hellospringmvc.dao;

import com.hellospringmvc.model.Event;
import com.hellospringmvc.model.Ticket;
import com.hellospringmvc.model.TicketImpl;
import com.hellospringmvc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TicketDao {

    private final ArrayList<TicketImpl> data;

    @Autowired
    public TicketDao(Storage storage) {
        this.data = (ArrayList<TicketImpl>) storage.getTickets();
    }

    public Ticket get(long id) {
        return data.stream()
                .filter(ticket -> ticket.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public List<Ticket> get(User user, int pageSize, int pageNum) {
        return data.stream()
                .filter(ticket -> ticket.getUserId() == user.getId())
                .skip(pageNum * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public List<Ticket> get(int pageSize, int pageNum) {
        return data.stream()
                .skip(pageNum * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public List<Ticket> get(Event event, int pageSize, int pageNum) {
        return data.stream()
                .filter(ticket -> ticket.getEventId() == event.getId())
                .skip(pageNum * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public Ticket create(long userId, long eventId, int place, Ticket.Category category) {
        TicketImpl ticket = new TicketImpl();
        ticket.setId(1 + (long) data.size());
        ticket.setUserId(userId);
        ticket.setEventId(eventId);
        ticket.setPlace(place);
        ticket.setCategory(category);
        data.add(ticket);
        return ticket;
    }

    public boolean delete(long ticketId) {
        boolean removed = data.removeIf(ticket -> ticket.getId() == ticketId);
        if (removed) {
            data.trimToSize();
        }
        return removed;
    }
}

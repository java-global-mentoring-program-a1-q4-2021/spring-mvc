package com.hellospringmvc.dto;

import com.hellospringmvc.model.Ticket;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TicketDto {
    private EventDto event;

    public TicketDto() {
    }

    public TicketDto(EventDto event, UserDto user, Ticket.Category category, int place) {
        this.event = event;
        this.user = user;
        this.category = category;
        this.place = place;
    }

    private UserDto user;
    private Ticket.Category category;
    private int place;

    @XmlElement(name = "event")
    public EventDto getEvent() {
        return event;
    }

    public void setEvent(EventDto event) {
        this.event = event;
    }

    @XmlElement(name = "user")
    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public Ticket.Category getCategory() {
        return category;
    }

    public void setCategory(Ticket.Category category) {
        this.category = category;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }
}

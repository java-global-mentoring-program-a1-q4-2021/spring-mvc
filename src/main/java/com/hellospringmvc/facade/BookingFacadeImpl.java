package com.hellospringmvc.facade;

import com.hellospringmvc.dto.TicketListDto;
import com.hellospringmvc.model.Event;
import com.hellospringmvc.model.EventImpl;
import com.hellospringmvc.model.Ticket;
import com.hellospringmvc.model.User;
import com.hellospringmvc.model.UserImpl;
import com.hellospringmvc.service.EventService;
import com.hellospringmvc.service.TicketService;
import com.hellospringmvc.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service("bookingFacade")
public class BookingFacadeImpl implements BookingFacade {

    private final EventService eventService;
    private final TicketService ticketService;
    private final UserService userService;
    private final TransactionTemplate transactionTemplate;

    public BookingFacadeImpl(EventService eventService,
                             TicketService ticketService,
                             UserService userService,
                             PlatformTransactionManager transactionManager) {
        this.eventService = eventService;
        this.ticketService = ticketService;
        this.userService = userService;
        this.transactionTemplate = new TransactionTemplate(transactionManager);
    }

    @Override
    public Event getEventById(long eventId) {
        return eventService.getOneById(eventId);
    }

    @Override
    public List<Event> getEventsByTitle(String title, int pageSize, int pageNum) {
        return eventService.getByTitle(title, pageSize, pageNum);
    }

    @Override
    public List<Event> getEventsForDay(Date day, int pageSize, int pageNum) {
        return eventService.getByDate(day, pageSize, pageNum);
    }

    @Override
    public List<Event> getEvents(int pageSize, int pageNum) {
        return eventService.getByDate(pageSize, pageNum);
    }

    @Override
    public Event createEvent(Event event) {
        return eventService.create(event);
    }

    @Override
    public Event updateEvent(Event event) {
        return eventService.update(event);
    }

    @Override
    public boolean deleteEvent(long eventId) {
        return eventService.delete(eventId);
    }

    @Override
    public User getUserById(long userId) {
        return userService.getUserById(userId);
    }

    @Override
    public User getUserByEmail(String email) {
        return userService.getUserByEmail(email);
    }

    @Override
    public List<User> getUsersByName(String name, int pageSize, int pageNum) {
        return userService.getUsersByName(name, pageSize, pageNum);
    }

    @Override
    public List<User> getUsers(int pageSize, int pageNum) {
        return userService.getUsers(pageSize, pageNum);
    }

    @Override
    public User createUser(User user) {
        return userService.createUser(user);
    }

    @Override
    public User updateUser(User user) {
        return userService.updateUser(user);
    }

    @Override
    public boolean deleteUser(long userId) {
        return userService.deleteUser(userId);
    }

    @Override
    public Ticket bookTicket(long userId, long eventId, int place, Ticket.Category category) {
        return ticketService.bookTicket(userId, eventId, place, category);
    }

    @Override
    public Ticket getTicketById(long id) {
        return ticketService.getTicket(id);
    }

    @Override
    public List<Ticket> getBookedTickets(User user, int pageSize, int pageNum) {
        return ticketService.getBookedTickets(user, pageSize, pageNum);
    }

    @Override
    public List<Ticket> getBookedTickets(Event event, int pageSize, int pageNum) {
        return ticketService.getBookedTickets(event, pageSize, pageNum);
    }

    @Override
    public List<Ticket> getTickets(int pageSize, int pageNum) {
        return ticketService.getTickets(pageSize, pageNum);
    }

    @Override
    public boolean cancelTicket(long ticketId) {
        return ticketService.cancelTicket(ticketId);
    }

    @Override
    public void loadTickets(MultipartFile xmlFile) throws Exception {
        final Set<Exception> exceptions = new HashSet<>();
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                try {
                    TicketListDto dto = fromXmlFile(xmlFile);
                    if (!CollectionUtils.isEmpty(dto.getTickets())) {
                        dto.getTickets().forEach(t -> {
                            Event event = new EventImpl();
                            event.setTitle(t.getEvent().getTitle());
                            event.setDate(t.getEvent().getDate());
                            event = eventService.create(event);

                            User user = new UserImpl();
                            user.setName(t.getUser().getName());
                            user.setEmail(t.getUser().getEmail());
                            user = userService.createUser(user);

                            ticketService.bookTicket(user.getId(), event.getId(), t.getPlace(), t.getCategory());
                        });
                    }
                } catch (Exception e) {
                    status.setRollbackOnly();
                    exceptions.add(e);
                }
            }
        });
        if(exceptions.isEmpty()){
            return;
        }
        throw exceptions.iterator().next();
    }

    private TicketListDto fromXmlFile(MultipartFile xmlFile) throws IOException, JAXBException {
        TicketListDto result;
        try (InputStream is = xmlFile.getInputStream()) {
            JAXBContext context = JAXBContext.newInstance(TicketListDto.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            result = (TicketListDto) unmarshaller.unmarshal(is);
        }
        return result;
    }

    private File toXmlFile(TicketListDto dto) throws IOException, JAXBException {
        File file = File.createTempFile("tickets", ".xml");
        JAXBContext context = JAXBContext.newInstance(TicketListDto.class);
        Marshaller mar = context.createMarshaller();
        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mar.marshal(dto, file);
        return file;
    }
}

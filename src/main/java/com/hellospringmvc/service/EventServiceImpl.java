package com.hellospringmvc.service;

import com.hellospringmvc.dao.EventDao;
import com.hellospringmvc.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class EventServiceImpl implements EventService {
    @Autowired
    private EventDao eventDao;

    @Override
    public Event getOneById(long eventId) {
        return eventDao.get(eventId);
    }

    @Override
    public List<Event> getByTitle(String title, int pageSize, int pageNum) {
        return eventDao.get(title, pageSize, pageNum);
    }

    @Override
    public List<Event> getByDate(Date day, int pageSize, int pageNum) {
        return eventDao.get(day, pageSize, pageNum);
    }

    @Override
    public Event create(Event event) {
        return eventDao.create(event);
    }

    @Override
    public Event update(Event event) {
        return eventDao.update(event);
    }

    @Override
    public boolean delete(long eventId) {
        return eventDao.delete(eventId);
    }

    @Override
    public List<Event> getByDate(int pageSize, int pageNum) {
        return eventDao.get(pageSize, pageNum);
    }
}

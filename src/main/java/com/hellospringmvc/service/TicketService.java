package com.hellospringmvc.service;

import com.hellospringmvc.model.Event;
import com.hellospringmvc.model.Ticket;
import com.hellospringmvc.model.User;

import java.util.List;

public interface TicketService {
    Ticket bookTicket(long userId, long eventId, int place, Ticket.Category category);

    List<Ticket> getBookedTickets(User user, int pageSize, int pageNum);

    List<Ticket> getBookedTickets(Event event, int pageSize, int pageNum);

    boolean cancelTicket(long ticketId);

    List<Ticket> getTickets(int pageSize, int pageNum);

    Ticket getTicket(long ticketId);
}

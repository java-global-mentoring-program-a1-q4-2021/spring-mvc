package com.hellospringmvc.service;

import com.hellospringmvc.dao.TicketDao;
import com.hellospringmvc.model.Event;
import com.hellospringmvc.model.Ticket;
import com.hellospringmvc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketServiceImpl implements TicketService {
    @Autowired
    private TicketDao ticketDao;

    @Override
    public Ticket bookTicket(long userId, long eventId, int place, Ticket.Category category) {
        return ticketDao.create(userId, eventId, place, category);
    }

    @Override
    public List<Ticket> getBookedTickets(User user, int pageSize, int pageNum) {
        return ticketDao.get(user, pageSize, pageNum);
    }

    @Override
    public List<Ticket> getBookedTickets(Event event, int pageSize, int pageNum) {
        return ticketDao.get(event, pageSize, pageNum);
    }

    @Override
    public boolean cancelTicket(long ticketId) {
        return ticketDao.delete(ticketId);
    }

    @Override
    public List<Ticket> getTickets(int pageSize, int pageNum) {
        return ticketDao.get(pageSize, pageNum);
    }

    @Override
    public Ticket getTicket(long ticketId) {
        return ticketDao.get(ticketId);
    }
}

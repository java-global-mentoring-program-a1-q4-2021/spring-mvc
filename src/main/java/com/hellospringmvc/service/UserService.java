package com.hellospringmvc.service;

import com.hellospringmvc.model.User;

import java.util.List;

public interface UserService {
    User getUserById(long userId);

    User getUserByEmail(String email);

    List<User> getUsersByName(String name, int pageSize, int pageNum);

    List<User> getUsers(int pageSize, int pageNum);

    User createUser(User user);

    User updateUser(User user);

    boolean deleteUser(long userId);
}

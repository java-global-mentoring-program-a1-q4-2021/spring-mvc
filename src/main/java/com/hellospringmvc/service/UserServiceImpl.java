package com.hellospringmvc.service;

import com.hellospringmvc.dao.UserDao;
import com.hellospringmvc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Override
    public User getUserById(long userId) {
        return userDao.get(userId);
    }

    @Override
    public User getUserByEmail(String email) {
        return userDao.get(email);
    }

    @Override
    public List<User> getUsersByName(String name, int pageSize, int pageNum) {
        return userDao.get(name, pageSize, pageNum);
    }

    @Override
    public List<User> getUsers(int pageSize, int pageNum) {
        return userDao.get(pageSize, pageNum);
    }

    @Override
    public User createUser(User user) {
        return userDao.create(user);
    }

    @Override
    public User updateUser(User user) {
        return userDao.update(user);
    }

    @Override
    public boolean deleteUser(long userId) {
        return userDao.delete(userId);
    }
}
